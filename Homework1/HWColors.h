//
//  HWColors.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 27/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#ifndef HWColors_h
#define HWColors_h

static NSInteger const HWLeafColorHex = 0x83AE84;
static NSInteger const HWCalypsoColorHex = 0x305E8F;
static NSInteger const HWGrayColorHex = 0xA9A9A9;

#endif /* HWColors_h */
