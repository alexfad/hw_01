//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    
    long **matrix = calloc(rows, sizeof(long*));
    
    for(int i = 0; i < rows; ++i)
    {
        matrix[i] = calloc(columns, sizeof(long));
    }
    
    
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < columns; j++)
        {
            matrix[i][j] = rand() % 200 - 100;
        }
    }
    
    return matrix;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    
    char *tempStr = calloc(500, sizeof(char*));
    
    for (int i = 0; i < strlen(numberString); i++)
    {
        if (numberString[i] == '.')
        {
            strcat(tempStr, "dot ");
        }
        
        else
        {
        
            switch(numberString[i])
            {
                case '0': strcat(tempStr, "zero "); break;
                case '1': strcat(tempStr, "one "); break;
                case '2': strcat(tempStr, "two "); break;
                case '3': strcat(tempStr, "three "); break;
                case '4': strcat(tempStr, "four "); break;
                case '5': strcat(tempStr, "five "); break;
                case '6': strcat(tempStr, "six "); break;
                case '7': strcat(tempStr, "seven "); break;
                case '8': strcat(tempStr, "eight "); break;
                case '9': strcat(tempStr, "nine "); break;
                default: strcat(tempStr, "wrong symbol"); break;
            }
        }
    }
    
    return tempStr;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */


HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    
    static float radius = 0.0;
    float speed = 7.5;
    
    float x0, y0, x, y;
    
    if (!radius && time < 1)
    {
        radius = canvasSize / 2.0;
    }
    
    else
    {
        radius -= 0.5;
    }
    
    if (radius <= (canvasSize / 2.0) * (-1))
    {
        radius = canvasSize / 2.0;
    }
    
    
    x0 = y0 = canvasSize / 2.0;
    x = x0 + radius * cos(-time * speed);
    y = y0 + radius * sin(-time * speed);
    
    return (HWPoint){x, y};
}

